import pytest
import click
import confuse
import types

from click.testing import CliRunner
from pathlib import Path

import clickset
import clickset.config
from clickset import ClickParams
from clickset import Setting
from clickset import get_config

# The class clickset.Setting contains persistent class data
# ... This is part of the magic that makes this package possible
# ... But for testing, the persistent data must be cleared for each test
# ... Similarly, any singleton configurations must be cleared after each test
@pytest.fixture
def clean_clickset():
    yield None

    # Clear the Setting class data
    clickset.Setting._click_options.clear()
    assert len(clickset.Setting._click_options) == 0

    # Clear the Configuration singletons
    clickset.config._configs.clear()

def test_clickset_exports():
    assert hasattr(clickset, 'Setting')
    assert hasattr(clickset, 'get_config')
    assert hasattr(clickset, 'ClickParams')

def test_setting_memory_only(clean_clickset):
    p_basic = 'foo'
    class TestClass:
        basic = Setting(None)

    obj = TestClass()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path is None
    assert TestClass.basic._config is None
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default is None
    assert TestClass.basic._value is None
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 0
    assert TestClass.basic._as is None

    obj.basic = 2

    assert obj.basic == 2

    obj.basic = 'hello world'

    assert obj.basic == 'hello world'

def test_setting_memory_only_click(clean_clickset):
    p_basic = 'foo'
    class TestClass:
        basic = Setting(
            None,
            default = '42',
            option = ClickParams('-t', '--test')
        )

    obj = TestClass()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path is None
    assert TestClass.basic._config is None
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default is '42'
    assert TestClass.basic._value is '42'
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 1
    assert TestClass.basic._as is None
    assert obj.basic == '42'

    ns = types.SimpleNamespace()
    ns.kw = None

    @click.command
    @Setting.options
    def main(**kw):
        ns.kw = kw

    runner = CliRunner()
    runner.invoke(main, ['-t', '69'])

    assert obj.basic == '69'


def test_setting_confuse_only(clean_clickset):
    p_basic = 'foo'
    class TestClass:
        basic = Setting(p_basic)

    config = get_config()

    obj = TestClass()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path == p_basic
    assert isinstance(TestClass.basic._config, clickset.config.ConfigPtr)
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default is None
    assert isinstance(TestClass.basic._value, confuse.Subview)
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 0
    assert callable(TestClass.basic._as)

    assert len(clickset.config._configs) == 1
    assert 'default' in clickset.config._configs

    with pytest.raises(confuse.NotFoundError):
        val = obj.basic

    assert all(p_basic not in src for src in config.sources)

    obj.basic = 2

    assert obj.basic == 2
    assert obj.basic == config[p_basic].get()
    assert p_basic in config.sources[0]
    assert all(p_basic not in src for src in config.sources[1:])

    obj.basic = 'FEEDFEFE'

    assert obj.basic == 'FEEDFEFE'
    assert obj.basic == config[p_basic].get()
    assert p_basic in config.sources[0]
    assert all(p_basic not in src for src in config.sources[1:])

def test_ClickParams():
    args   = ('-t', '--test')
    kwargs = {'opt1': 'DEAD', 'opt2': 'BEEF'}

    params = ClickParams(*args, **kwargs)

    assert params.args   == args
    assert params.kwargs == kwargs

def test_setting_click_option_only(clean_clickset):
    p_basic = 'foo'
    class TestClass:
        basic = Setting(
            None,
            option = ClickParams('-t', '--test')
        )

    obj = TestClass()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path is None
    assert TestClass.basic._config is None
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default is None
    assert TestClass.basic._value is None
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 1
    assert len(Setting._click_options) == 1
    assert Setting._click_options == TestClass.basic._click_options
    assert TestClass.basic._as is None
    assert all(callable(opt) for opt in Setting._click_options)

def test_setting_click_and_confuse(clean_clickset):
    p_basic = 'foo'
    class TestClass:
        basic = Setting(
            p_basic,
            option = ClickParams('-t', '--test')
        )

    obj = TestClass()

    config = get_config()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path == p_basic
    assert isinstance(TestClass.basic._config, clickset.config.ConfigPtr)
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default is None
    assert isinstance(TestClass.basic._value, confuse.Subview)
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 1
    assert len(Setting._click_options) == 1
    assert Setting._click_options == TestClass.basic._click_options
    assert callable(TestClass.basic._as)
    assert all(callable(opt) for opt in Setting._click_options)

    with pytest.raises(confuse.NotFoundError):
        obj.basic

    assert all(p_basic not in src for src in config.sources)

    ns = types.SimpleNamespace()
    ns.kw = None

    @click.command
    @Setting.options
    def main(**kw):
        ns.kw = kw

    runner = CliRunner()
    runner.invoke(main, ['-t', 'FEEDFEFE'])

    assert ns.kw == {'test': 'FEEDFEFE'}
    assert obj.basic == 'FEEDFEFE'
    assert obj.basic == config[p_basic].get()
    assert all(p_basic not in src for src in config.sources if not isinstance(src, clickset.config.CliSource))
    assert all(p_basic     in src for src in config.sources if     isinstance(src, clickset.config.CliSource))

    runner = CliRunner()
    runner.invoke(main, ['--test', 'DEADBEEF'])

    assert ns.kw == {'test': 'DEADBEEF'}
    assert obj.basic == 'DEADBEEF'
    assert obj.basic == config[p_basic].get()
    assert all(p_basic not in src for src in config.sources if not isinstance(src, clickset.config.CliSource))
    assert all(p_basic     in src for src in config.sources if     isinstance(src, clickset.config.CliSource))

    obj.basic = 'DEADFEFE'

    assert obj.basic == 'DEADFEFE'
    assert obj.basic == config[p_basic].get()
    assert p_basic in config.sources[0]

    obj.basic = None
    assert obj.basic == None
    assert obj.basic == config[p_basic].get()


def test_setting_confuse_default(clean_clickset):
    p_basic = 'foo'
    default_val = 'DEADBEEF'
    class TestClass:
        basic = Setting(p_basic, default=default_val)

    obj = TestClass()
    config = get_config()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path == p_basic
    assert isinstance(TestClass.basic._config, clickset.config.ConfigPtr)
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default == default_val
    assert isinstance(TestClass.basic._value, confuse.Subview)
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 0
    assert callable(TestClass.basic._as)

    assert obj.basic == default_val
    assert all(p_basic not in src for src in config.sources[:-1])
    assert p_basic in config.sources[-1]

    obj.basic = 'FEEDFEFE'

    assert obj.basic != default_val
    assert obj.basic == 'FEEDFEFE'

    del obj.basic

    assert obj.basic == default_val

def test_setting_memory_only(clean_clickset):
    p_basic = 'foo'
    default_val = 'DEADBEEF'
    class TestClass:
        basic = Setting(None, default=default_val)

    obj = TestClass()

    assert hasattr(TestClass, 'basic')
    assert isinstance(TestClass.basic, Setting)
    assert TestClass.basic._path is None
    assert TestClass.basic._config is None
    assert TestClass.basic._name == 'basic'
    assert TestClass.basic._default == default_val
    assert TestClass.basic._value == default_val
    assert isinstance(TestClass.basic._click_options, list)
    assert len(TestClass.basic._click_options) == 0
    assert TestClass.basic._as is None

    assert obj.basic == default_val

    obj.basic = 'FEEDFEFE'

    assert obj.basic == 'FEEDFEFE'

    del obj.basic

    assert obj.basic == default_val


confuse_type_tests = [
    ('testfile.txt', Setting.Types.FILENAME, None,  str(Path.cwd()/'testfile.txt')),
    ('testfile.txt', Setting.Types.PATH,     None,  Path.cwd()/'testfile.txt'),
    (42,             Setting.Types.NUMBER,   None,  42),
    (42.42,          Setting.Types.NUMBER,   None,  42.42),

    ('testfile.txt', None, confuse.templates.Filename(in_app_dir=True), str(Path(get_config().user_config_path()).parent/'testfile.txt')),
    ('testfile.txt', None, confuse.templates.Path(in_app_dir=True), Path(get_config().user_config_path()).parent/'testfile.txt'),
]

@pytest.mark.parametrize("value,otype,targs,expected", confuse_type_tests)
def test_setting_click_type(clean_clickset, value, otype, targs, expected):
    class TestClass:
        value = Setting('testval', type=otype, get_args=targs)

    obj = TestClass()

    config = get_config()

    obj.value = value

    assert obj.value == expected

def test_config_ptr(clean_clickset):
    cname = "default"
    cptr = clickset.config.ConfigPtr()

    assert cptr.name == cname

    with pytest.raises(clickset.ConfigNotFound):
        assert cptr.value is None

    config = get_config()

    assert cptr.name == cname
    assert cptr.value == config

def test_config_ptr_named(clean_clickset):
    cname = "myconfig"
    cptr = clickset.config.ConfigPtr(cname)

    assert cptr.name == cname

    with pytest.raises(clickset.ConfigNotFound):
        assert cptr.value is None

    config = get_config(cname)

    assert cptr.name == cname
    assert cptr.value == config

def test_config_get(clean_clickset):
    p_basic = 'foo'
    default_val = 'DEADBEEF'

    class TestClass:
        basic = Setting(
            p_basic,
            default=default_val,
        )

    config = get_config()

    assert TestClass.basic.get() == default_val

def test_config_get_with_args(clean_clickset):
    p_basic = 'foo'
    default_val = 'DEADBEEF'

    class TestClass:
        basic = Setting(
            p_basic,
            default  = default_val,
            get_args = confuse.templates.Path(in_app_dir=True),
        )

    config = get_config()

    assert TestClass.basic.get() == Path(config.config_dir())/default_val

def test_config_setting_direct(clean_clickset):
    p_basic = 'foo'
    default_val = 'DEADBEEF'

    basic = Setting(
        p_basic,
        default  = default_val,
        get_args = confuse.templates.Path(in_app_dir=True),
    )

    config = get_config()

    assert basic.get() == Path(config.config_dir())/default_val
