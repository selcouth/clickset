import pytest
import confuse
import logging
import os

from unittest import mock

import clickset

from clickset import get_config

# Any singleton configurations must be cleared after each test
@pytest.fixture
def clean_config():
    yield None

    # Clear the Configuration singletons
    clickset.config._configs.clear()

def test_config_appname_change(clean_config, caplog):
    config = get_config()
    assert config.appname == 'clickset'
    with caplog.at_level(logging.WARNING):
        config2 = get_config(appname='testapp')
    assert config2.appname == 'testapp'
    assert config.appname == config2.appname
    assert 'appname changed' in caplog.text

def test_config_modname_change(clean_config, caplog):
    config = get_config()
    assert config.modname is None
    with caplog.at_level(logging.WARNING):
        config2 = get_config(modname='testmod')
    assert config2.modname == 'testmod'
    assert config.modname == config2.modname
    assert 'modname changed' in caplog.text

def test_config_source_types(clean_config):
    config = get_config()

    assert isinstance(config, clickset.config.Configuration)
    assert len(config.sources) == 3

    assert isinstance(config.sources[0],     clickset.config.MemorySource)
    assert isinstance(config.sources[1],     clickset.config.CliSource)
    assert isinstance(config.sources[2],     clickset.config.AppSource)
    assert isinstance(config.source_memory,  clickset.config.MemorySource)
    assert isinstance(config.source_cli,     clickset.config.CliSource)
    assert isinstance(config.source_default, clickset.config.AppSource)

    assert config.sources[-1].default


def test_config_subview(clean_config):
    config = get_config()

    sv1 = config['key1']
    sv2 = config['key1']['key2']
    sv3 = sv1['key2']

    assert isinstance(sv1, clickset.config.Subview)
    assert isinstance(sv2, clickset.config.Subview)
    assert isinstance(sv3, clickset.config.Subview)

def test_config_deep_update(clean_config):
    config = get_config()
    view = config['key1']['key2']
    view.set_memory('FEEDFEFE')

    assert len(config.sources) == 3

    assert view.get() == 'FEEDFEFE'
    assert config.source_memory == {'key1': {'key2': 'FEEDFEFE'}}

    config._deep_update(config.source_memory, {'key1': {'key2': 'DEADBEEF'}})

    assert view.get() == 'DEADBEEF'


def test_config_delete(clean_config):
    config = get_config()
    view = config['key1']['key2']['key3']['key4']
    view.set_default('DEADBEEF')

    view2 = config['key1']['key2']['key3']['key5']
    view2.set_default(42)

    assert len(config.sources) == 3
    assert view.get() == 'DEADBEEF'

    view.set_memory('FEEDFEFE')

    assert len(config.sources) == 3
    assert view.get() == 'FEEDFEFE'
    assert view2.get() == 42
    assert config.source_memory == {'key1': {'key2': {'key3': {'key4': 'FEEDFEFE'}}}}

    view.delete_from_memory()

    assert view.get() == 'DEADBEEF'
    assert view2.get() == 42

    view2.delete_from_memory()

    assert view.get() == 'DEADBEEF'
    assert view2.get() == 42

def test_config_add(clean_config):
    config = get_config()
    view = config['key1']['key2']['key3']['key4']

    numsources = len(config.sources)

    assert config.source_default == {}
    view.add('DEADBEEF')

    # A new view should have been added just above in-memory default values
    # NOTE: See clickset.config.Configuration.add for why it is in this location
    assert len(config.sources) == numsources+1
    assert config.sources[-2] == {'key1': {'key2': {'key3': {'key4': 'DEADBEEF'}}}}

@pytest.fixture
def config_file_factory(tmp_path):
    def factory(content:dict, name='test.yaml'):
        config = clickset.config.Configuration('no_app', read=False)
        config.set_memory(content)
        filename = tmp_path/name
        with filename.open('w') as fd:
            fd.write(config.dump())
        return filename
    return factory

def test_config_set_file(clean_config, config_file_factory):
    # Create a test file
    content = {'FEEDFEFE': 'DEADBEEF'}
    filename = config_file_factory(content, 'test1.yaml')

    content2 = {'DEADFEFE': 'FEFEBEEF'}
    filename2 = config_file_factory(content2, 'test2.yaml')

    # Load the test file
    config = get_config()

    assert len(config.sources) == 3

    config.set_file(filename)

    assert len(config.sources) == 4
    assert isinstance(config.sources[-2], confuse.YamlSource)
    assert config['FEEDFEFE'].get() == 'DEADBEEF'

    config.set_file(filename2)

    assert len(config.sources) == 5
    assert isinstance(config.sources[-3], confuse.YamlSource)
    assert config['DEADFEFE'].get() == 'FEFEBEEF'

def test_config_add_file(clean_config, config_file_factory):
    # Create a test file
    content = {'FEEDFEFE': 'DEADBEEF'}
    filename = config_file_factory(content, 'test1.yaml')

    content2 = {'DEADFEFE': 'FEFEBEEF'}
    filename2 = config_file_factory(content2, 'test2.yaml')

    # Load the test file
    config = get_config()

    assert len(config.sources) == 3

    config.add_file(filename)

    assert len(config.sources) == 4
    assert isinstance(config.sources[-2], confuse.YamlSource)
    assert config['FEEDFEFE'].get() == 'DEADBEEF'

    config.add_file(filename2)

    assert len(config.sources) == 5
    assert isinstance(config.sources[-3], confuse.YamlSource)
    assert config['DEADFEFE'].get() == 'FEFEBEEF'

def test_config_save(clean_config, tmp_path):
    config1 = get_config()
    config1['FEEDFEFE'].set('DEADBEEF')
    filename = tmp_path/'test.yaml'

    assert not filename.exists()

    config1.save(filename)

    assert filename.exists()

    config2 = get_config('readback')

    config2.add_file(filename)

    assert config2['FEEDFEFE'].get() == 'DEADBEEF'

def test_config_read(clean_config, tmp_path):
    config1 = get_config()
    config1['FEEDFEFE'].set('DEADBEEF')
    filename = tmp_path/'config.yaml'

    assert not filename.exists()

    config1.save(filename)

    assert filename.exists()

    with mock.patch.dict(os.environ, {'READBACKDIR': str(tmp_path)}):
        config2 = get_config('readback', appname='readback', read=True)

    assert config2['FEEDFEFE'].get() == 'DEADBEEF'

def test_specialized_class(clean_config, tmp_path):
    class MyConfig(clickset.Configuration):
        pass

    config = get_config(configClass = MyConfig)
    assert isinstance(config, MyConfig)

def test_specialized_class_invalid(clean_config, tmp_path):
    class InvalidConfig:
        pass

    with pytest.raises(TypeError):
        config = get_config(configClass = InvalidConfig)
